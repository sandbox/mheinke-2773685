var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    rename     = require('gulp-rename'),
    prefixer   = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function () {
  gulp.src('../styles/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(prefixer())
    .pipe(rename('compile.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('../styles'));
});

gulp.task('default', ['sass'], function () {
  gulp.watch('../styles/scss/**/*.scss', ['sass']);
});
